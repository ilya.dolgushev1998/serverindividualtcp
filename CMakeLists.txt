cmake_minimum_required(VERSION 3.15)
project(untitled)

set(CMAKE_CXX_STANDARD 11)

add_executable(untitled server.cpp)

find_package(PostgreSQL REQUIRED)
target_include_directories(untitled PRIVATE ${PostgreSQL_INCLUDE_DIRS})

# Add libraries to link your target againts. Again, PRIVATE is important for multi-target projects
target_link_libraries(untitled PRIVATE ${PostgreSQL_LIBRARIES} pthread)
